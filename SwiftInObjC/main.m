//
//  main.m
//  SwiftInObjC
//
//  Created by Test OSX9 on 2016/2/3.
//  Copyright © 2016年 Test OSX9. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
