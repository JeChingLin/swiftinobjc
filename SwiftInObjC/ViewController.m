//
//  ViewController.m
//  SwiftInObjC
//
//  Created by Test OSX9 on 2016/2/3.
//  Copyright © 2016年 Test OSX9. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()<ButtonCellDelegate,UITableViewDataSource, UITableViewDelegate>
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
 
    UITableView *tableView = [[UITableView alloc]initWithFrame:self.view.frame style:UITableViewStylePlain];
    tableView.dataSource = self;
    tableView.delegate = self;
    [tableView registerClass:[ButtonCell class] forCellReuseIdentifier:@"buttonCell"];
    [self.view addSubview:tableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 99;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ButtonCell *cell = [tableView dequeueReusableCellWithIdentifier:@"buttonCell"];
    
    if (!cell) {
        cell = [[ButtonCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"buttonCell"];
    }

    cell.buttonDelegate = self;
    cell.tapButton.tag = indexPath.row;
    cell.textLabel.text = [NSString stringWithFormat:@"%ld", (long)indexPath.row];
    return cell;
}

-(void)cellTapped:(ButtonCell *)cell{
    UIButton *button = cell.tapButton;

    NSLog(@"buttonTag: %ld", (long)button.tag);
}

@end
