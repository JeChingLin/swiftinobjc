//
//  AppDelegate.h
//  SwiftInObjC
//
//  Created by Test OSX9 on 2016/2/3.
//  Copyright © 2016年 Test OSX9. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

