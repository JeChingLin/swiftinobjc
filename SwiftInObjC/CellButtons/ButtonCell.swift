//
//  ButtonCell.swift
//  CellButtons
//
//  Created by Jure Zove on 20/09/15.
//  Copyright © 2015 Candy Code. All rights reserved.
//

import UIKit

@objc
protocol ButtonCellDelegate {
    func cellTapped(cell: ButtonCell)
}

class ButtonCell: UITableViewCell {
    var buttonDelegate: ButtonCellDelegate?
    var tapButton: UIButton?
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        tapButton = UIButton.init(type: .Custom)
        tapButton!.frame = CGRectMake(2, 4, 100, 40)
        tapButton!.backgroundColor = UIColor.blackColor()
        tapButton?.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        tapButton!.setTitle("Tap!!!", forState: .Normal)
        tapButton!.addTarget(self, action: "buttonTap:", forControlEvents:.TouchUpInside)
        self.contentView .addSubview(tapButton!)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buttonTap(sender: AnyObject) {
        print(__FUNCTION__)
        if let delegate = buttonDelegate {
            delegate.cellTapped(self)
        }
    }
}
